<?php

use App\Http\Controllers\UserBalanceController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserTranslationController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::resource('users', UserController::class)->only([
    'update'
]);

Route::put('users/{user}/balances', [UserBalanceController::class, 'update'])->name('users.balances.update');

Route::post('users/{user}/transactions', [UserTranslationController::class, 'store'])->name('users.transactions.store');
