<?php

namespace App\Bank\Dto;

use App\Http\Requests\BalanceUpdateRequest;

class UserBalanceUpdateDto
{
    private const MULTIPLIER = 100;

    public float $amount;

    public static function fromRequest(BalanceUpdateRequest $request): self
    {
        $dto = new self();
        $dto->amount = $request->input('data.amount') * self::MULTIPLIER;

        return $dto;
    }
}
