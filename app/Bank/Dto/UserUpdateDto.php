<?php

namespace App\Bank\Dto;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateDto
{
    public string $name;

    public string $email;

    public int $age;

    public static function fromRequest(FormRequest $request): self
    {
        $dto = new self();
        $dto->name = $request->input('data.name');
        $dto->email = $request->input('data.email');
        $dto->age = $request->input('data.age');

        return $dto;
    }
}
