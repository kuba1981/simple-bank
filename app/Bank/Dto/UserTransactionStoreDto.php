<?php

namespace App\Bank\Dto;

use App\Http\Requests\TransactionStoreRequest;

class UserTransactionStoreDto
{
    public int $recipientId;

    public float $amount;

    public static function fromRequest(TransactionStoreRequest $request): self
    {
        $dto = new self();
        $dto->recipientId = $request->input('data.to');
        $dto->amount = $request->input('data.amount') * 100;

        return $dto;
    }
}
