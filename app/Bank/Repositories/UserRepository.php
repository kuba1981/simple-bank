<?php

namespace App\Bank\Repositories;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class UserRepository
{
    public function updateById(int $id, array $values): bool
    {
        return User::findOrFail($id)?->update($values);
    }

    public function getById(int $id): User
    {
        return User::lockForUpdate()->findOrFail($id);
    }

    public function incrementBalanceById(int $id, int $amount): bool
    {
        return User::findOrFail($id)?->increment('balance', $amount);
    }

    public function decrementBalanceById(int $id, int $amount): bool
    {
        return User::findOrFail($id)?->decrement('balance', $amount);
    }
}
