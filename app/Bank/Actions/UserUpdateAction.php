<?php

namespace App\Bank\Actions;

use App\Bank\Dto\UserUpdateDto;
use App\Bank\Repositories\UserRepository;

readonly class UserUpdateAction
{
    public function __construct(private UserRepository $userRepository)
    {
        //
    }

    public function execute(int $id, UserUpdateDto $dto): void
    {
        $this->userRepository->updateById($id, [
            'name' => $dto->name,
            'email' => $dto->email,
            'age' => $dto->age,
        ]);
    }
}
