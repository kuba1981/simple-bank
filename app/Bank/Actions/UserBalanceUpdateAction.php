<?php

namespace App\Bank\Actions;

use App\Bank\Dto\UserBalanceUpdateDto;
use App\Bank\Repositories\UserRepository;

readonly class UserBalanceUpdateAction
{
    public function __construct(private UserRepository $userRepository)
    {
    }

    public function execute(int $id, UserBalanceUpdateDto $dto): void
    {
        $this->userRepository->incrementBalanceById($id, $dto->amount,);
    }
}
