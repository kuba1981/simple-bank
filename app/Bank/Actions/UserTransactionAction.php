<?php

namespace App\Bank\Actions;

use App\Bank\Dto\UserTransactionStoreDto;
use App\Bank\Repositories\UserRepository;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

readonly class UserTransactionAction
{
    public function __construct(private UserRepository $userRepository)
    {
    }

    /**
     * @param int $id
     * @param UserTransactionStoreDto $dto
     * @return User
     */
    public function execute(int $id, UserTransactionStoreDto $dto): User
    {
        return DB::transaction(function () use ($id, $dto) {
            $user = $this->userRepository->getById($id);

            if (!$this->hasAvailableBalance($user, $dto->amount)) {
                $this->throwValidationException(
                    'You do not have sufficient funds to complete this operation. Please recharge your balance.'
                );
            }

            $user->balance -= $dto->amount;
            $user->save();

            $this->userRepository->incrementBalanceById($dto->recipientId, $dto->amount);

            return $user;
        });
    }

    private function hasAvailableBalance(User $user, int $amount): bool
    {
        return $user->balance - $amount > 0;
    }

    /**
     * @throws ValidationException
     */
    private function throwValidationException(string $message): void
    {
        throw ValidationException::withMessages(
            ['error' => $message]
        );
    }
}
