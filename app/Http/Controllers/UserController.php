<?php

namespace App\Http\Controllers;

use App\Bank\Actions\UserUpdateAction;
use App\Http\Requests\UserUpdateRequest;
use Illuminate\Http\Response;

class UserController extends Controller
{
    public function __construct(private readonly UserUpdateAction $action)
    {
    }

    public function update(int $id, UserUpdateRequest $request): Response
    {
        $this->action->execute($id, $request->dto());

        return response()->noContent();
    }
}
