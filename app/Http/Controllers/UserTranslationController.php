<?php

namespace App\Http\Controllers;

use App\Bank\Actions\UserTransactionAction;
use App\Http\Requests\TransactionStoreRequest;
use App\Http\Resources\UserResource;

class UserTranslationController extends Controller
{
    public function __construct(private readonly UserTransactionAction $action)
    {
    }

    public function store(int $id, TransactionStoreRequest $request): UserResource
    {
        $user = $this->action->execute($id, $request->dto());

        return new UserResource($user);
    }
}
