<?php

namespace App\Http\Controllers;

use App\Bank\Actions\UserBalanceUpdateAction;
use App\Http\Requests\BalanceUpdateRequest;
use Illuminate\Http\Response;

class UserBalanceController extends Controller
{
    public function __construct(private readonly UserBalanceUpdateAction $action)
    {
    }

    public function update(int $id, BalanceUpdateRequest $request): Response
    {
        $this->action->execute($id, $request->dto());

        return response()->noContent();
    }
}
