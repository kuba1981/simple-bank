<?php

namespace App\Http\Requests;

use App\Bank\Dto\UserUpdateDto;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'data.name' => 'required|string|max:255',
            'data.email' => [
                'required',
                'email',
                Rule::unique('users', 'email')->ignore($this->route('user'))
            ],
            'data.age' => 'required|integer|min:18'
        ];
    }

    public function dto(): UserUpdateDto
    {
        return UserUpdateDto::fromRequest($this);
    }
}
