<?php

namespace App\Http\Requests;

use App\Bank\Dto\UserTransactionStoreDto;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Foundation\Http\FormRequest;

class TransactionStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'data.to' => 'required|integer|exists:users,id',
            'data.amount' => 'required|numeric|gt:0',
        ];
    }

    public function dto(): UserTransactionStoreDto
    {
        return UserTransactionStoreDto::fromRequest($this);
    }
}
