## Up project

Для локальной разработки использовался https://laradock.io/getting-started/

После установки laradock

- ** docker-compose up workspace php-fpm nginx postgres
- ** docker-compose exec workspace bash
- ** composer install
- ** php artisan test

## API
- PUT|PATCH api/users/{user}
- PUT api/users/{user}/balances
- POST api/users/{user}/transactions
