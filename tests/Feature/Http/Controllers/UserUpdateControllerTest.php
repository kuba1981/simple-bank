<?php

namespace Tests\Feature\Http\Controllers;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class UserUpdateControllerTest extends TestCase
{
    public function testUpdateThrow404(): void
    {
        $this->putJson(route('users.update', [1]), [
            'data' => [
                'name' => 'test 2',
                'email' => 'test2@gmail.com',
                'age' => 30
            ]
        ])->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /**
     * @dataProvider updateThrow422Provider
     *
     * @param array $request
     * @param array $errors
     * @return void
     */
    public function testUpdateThrow422(array $request, array $errors): void
    {
        $this->putJson(route('users.update', [1]), $request)->assertStatus(
            Response::HTTP_UNPROCESSABLE_ENTITY
        )->assertJson($errors);
    }

    public static function updateThrow422Provider(): array
    {
        return [
            [
                'request' => [],
                'errors' => [
                    'errors' => [
                        'data.name' => [
                            'The data.name field is required.'
                        ],
                        'data.email' => [
                            'The data.email field is required.'
                        ],
                        'data.age' => [
                            'The data.age field is required.'
                        ]
                    ]
                ]
            ],
            [
                'request' => [
                    'data' => [
                        'name' => 'test',
                        'email' => 'test',
                        'age' => 'test'
                    ]
                ],
                'errors' => [
                    'errors' => [
                        'data.email' => [
                            'The data.email field must be a valid email address.'
                        ],
                        'data.age' => [
                            'The data.age field must be an integer.',
                            'The data.age field must be at least 18.'
                        ]
                    ]
                ]
            ]
        ];
    }

    public function testUpdateSuccess(): void
    {
        $user = User::factory()->create([
            'name' => 'test',
            'email' => 'test@gmail.com',
            'age' => 35
        ]);

        $this->putJson(uri: route('users.update', [$user->id]), data: [
            'data' => [
                'name' => 'test 2',
                'email' => 'test2@gmail.com',
                'age' => 30
            ]
        ])->assertStatus(
            Response::HTTP_NO_CONTENT
        );

        $this->assertDatabaseHas('users', [
            'name' => 'test 2',
            'email' => 'test2@gmail.com',
            'age' => 30
        ]);
    }
}
