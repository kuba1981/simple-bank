<?php

namespace Tests\Feature\Http\Controllers;

use App\Models\User;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class UserBalanceControllerTest extends TestCase
{
    /**
     * @dataProvider updateThrow422Provider
     *
     * @param array $request
     * @param array $errors
     * @return void
     */
    public function testUpdateThrow422(array $request, array $errors): void
    {
        $this->putJson(route('users.balances.update', [1]), $request)->assertStatus(
            Response::HTTP_UNPROCESSABLE_ENTITY
        )->assertJson($errors);
    }

    public static function updateThrow422Provider(): array
    {
        return [
            [
                'request' => [],
                'errors' => [
                    'errors' => [
                        'data.amount' => [
                            'The data.amount field is required.'
                        ],
                    ]
                ]
            ],
            [
                'request' => [
                    'data' => [
                        'amount' => 'test',
                    ]
                ],
                'errors' => [
                    'errors' => [
                        'data.amount' => [
                            'The data.amount field must be a number.'
                        ],
                    ]
                ]
            ],
            [
                'request' => [
                    'data' => [
                        'amount' => 0,
                    ]
                ],
                'errors' => [
                    'errors' => [
                        'data.amount' => [
                            'The data.amount field must be greater than 0.'
                        ],
                    ]
                ]
            ],
        ];
    }

    public function testUpdateSuccess(): void
    {
        $user = User::factory()->create([
            'name' => 'test',
            'email' => 'test@gmail.com',
            'age' => 35,
            'balance' => 3500
        ]);

        $this->putJson(uri: route('users.balances.update', [$user->id]), data: [
            'data' => [
                'amount' => 45.50,
            ]
        ])->assertStatus(
            Response::HTTP_NO_CONTENT
        );

        $this->assertDatabaseHas('users', [
            'balance' => 8050,
        ]);
    }
}
