<?php

namespace Feature\Http\Controllers;

use App\Models\User;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class UserTranslationControllerTest extends TestCase
{
    public function testUpdateThrow404(): void
    {
        $to = User::factory()->create([
            'name' => 'test',
            'email' => 'test@gmail.com',
            'age' => 35,
            'balance' => 3500
        ]);

        $this->postJson(route('users.transactions.store', [1]), [
            'data' => [
                'to' => $to->id,
                'amount' => 4500,
            ]
        ])->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /**
     * @dataProvider updateThrow422Provider
     *
     * @param array $request
     * @param array $errors
     * @return void
     */
    public function testStoreThrow422(array $request, array $errors): void
    {
        $this->postJson(route('users.transactions.store', [1]), $request)->assertStatus(
            Response::HTTP_UNPROCESSABLE_ENTITY
        )->assertJson($errors);
    }

    public static function updateThrow422Provider(): array
    {
        return [
            [
                'request' => [],
                'errors' => [
                    'errors' => [
                        'data.to' => [
                            'The data.to field is required.'
                        ],
                        'data.amount' => [
                            'The data.amount field is required.'
                        ],
                    ]
                ]
            ],
            [
                'request' => [
                    'data' => [
                        'to' => 'test',
                        'amount' => 'test',
                    ]
                ],
                'errors' => [
                    'errors' => [
                        'data.to' => [
                            'The data.to field must be an integer.'
                        ],
                        'data.amount' => [
                            'The data.amount field must be a number.',
                            'The data.amount field must be greater than 0.'
                        ],
                    ]
                ]
            ],
            [
                'request' => [
                    'data' => [
                        'to' => 2,
                        'amount' => 0,
                    ]
                ],
                'errors' => [
                    'errors' => [
                        'data.to' => [
                            'The selected data.to is invalid.'
                        ],
                        'data.amount' => [
                            'The data.amount field must be greater than 0.'
                        ],
                    ]
                ]
            ],
        ];
    }

    public function testStoreThrowInsufficientBalanceException(): void
    {
        $user = User::factory()->create([
            'name' => 'test',
            'email' => 'test@gmail.com',
            'age' => 35,
            'balance' => 3500
        ]);

        $to = User::factory()->create([
            'name' => 'test 2',
            'email' => 'test2@gmail.com',
            'age' => 45,
            'balance' => 2000
        ]);

        $this->postJson(uri: route('users.transactions.store', [$user->id]), data: [
            'data' => [
                'to' => $to->id,
                'amount' => 45.50,
            ]
        ])->assertStatus(
            Response::HTTP_UNPROCESSABLE_ENTITY
        )->assertJson([
            'message' => 'You do not have sufficient funds to complete this operation. Please recharge your balance.',
            'errors' => [
                'error' => [
                    'You do not have sufficient funds to complete this operation. Please recharge your balance.'
                ]
            ]
        ]);
    }

    public function testStoreSuccess(): void
    {
        $from = User::factory()->create([
            'name' => 'test',
            'email' => 'test@gmail.com',
            'age' => 35,
            'balance' => 9500
        ]);

        $to = User::factory()->create([
            'name' => 'test 2',
            'email' => 'test2@gmail.com',
            'age' => 45,
            'balance' => 2100
        ]);

        $this->postJson(uri: route('users.transactions.store', [$from->id]), data: [
            'data' => [
                'to' => $to->id,
                'amount' => 45.50,
            ]
        ])->assertStatus(
            Response::HTTP_OK
        )->assertJson([
            'data' => [
                'id' => $from->id,
                'email' => $from->email,
                'age' => $from->age,
                'balance' => 49.50,
            ]
        ]);

        $this->assertDatabaseHas('users', [
            'id' => $from->id,
            'balance' => 4950,
        ]);

        $this->assertDatabaseHas('users', [
            'id' => $to->id,
            'balance' => 6650,
        ]);
    }
}
